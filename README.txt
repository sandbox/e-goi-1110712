$Id:
This module provides integration with the E-goi: Multichannel Marketing Platform (Email, SMS, MMS, Voice Broadcasting, Fax and Social Media). This module is still in active development. Activate the module through drupals administrative interface.
  
IMPORTANT: Please ensure you have an E-goi Account