<?php

class EGOIAPI {
	
	
	
	/**
	 * E-GOI settings
	 **/ 
	
	var $CONFIG = 
	
	array(
	
		'api_key'				=> false,
		'api_key_valid'			=> false,
		'api_version' 			=> 'v2',
		'api_url'				=> 'http://api.e-goi.com/',
		'api_url2'				=> '/soap.php?wsdl',
		'fields_type'			=> 'texto',
		'bo_url'				=> 'http://bo.29.e-goi.com/',
		'lang'					=> false,
		'listid'				=> false,
		'plugin_key'			=> '7df4fa23fb8d28da3aefa3f2ce7f6bf5',
		'settings_exists' 		=> false,
		'table_settings' 		=> 'egoi_configuration'
	
	);
	
	var $SOAP;
	
	/**
	 * End E-GOI settings
	 **/ 
	
	
	
	/**
	 * E-GOI new SOAP
	 **/ 
	function egoi_new_soap() {
		
		$this->SOAP = new SoapClient($this->CONFIG['api_url'].$this->CONFIG['api_version'].$this->CONFIG['api_url2']);
		
	}
	/**
	 * End E-GOI new SOAP
	 **/ 
	
	
	
	
	/**
	 * E-GOI verify API Key
	 **/ 
	function egoi_verify_api_key($key) {

		$params = array( 'apikey' => $key);
		$result = $this->SOAP->getClientData($params);
		return (!(isset($result['ERROR']))) ? true:false; 
		
	}
	/**
	 * End E-GOI verify API Key
	 **/ 
	
	
	
	/**
	 * E-GOI verify List name
	 **/ 
	function egoi_list_exists($key,$list) {
	
		$params = array( 'apikey' => $this->CONFIG['api_key'] );
		$result = $this->SOAP->getLists($params);
		
		$list_exists = false;
		foreach($result as $value) {
			if($value['title'] == $list)
				$list_exists = true;
		}
		
		return $list_exists ? true:false;
		
	}
	/**
	 * E-GOI verify List name
	 **/
	 
	 
	 
	 /**
	 * E-GOI create a list
	 **/ 
	function egoi_create_list($list) {
	
		$lang_array = array( 
								0 => 'de',
								1 => 'en',
								2 => 'es',
								3 => 'fr',
								4 => 'br',
								5 => 'pt'
					
		);
				
		if(!(array_key_exists($this->CONFIG['lang'], $lang_array))) {
			return false;
		}
	
		$params = array(
						'apikey' 		=> $this->CONFIG['api_key'],
						'plugin_key'	=> $this->CONFIG['plugin_key'],
						'nome' 			=> $list,
						'idioma_lista' 	=> $this->CONFIG['lang'],
						'canal_email' 	=> 1,
						'canal_sms' 	=> 0,
						'canal_fax' 	=> 0,
						'canal_voz' 	=> 0,
						'canal_mms' 	=> 0 );
						
		$result = $this->SOAP->createList($params);
		
		if(isset($result['ERROR'])) {
			return false;
		}
		
		return is_array($result) && isset($result['LIST_ID']) ? $result:false;
		
	}
	/**
	 * End E-GOI create a list
	 **/  
	
	
	
	/**
	 * E-GOI create extra fields
	 **/ 
	function egoi_create_extra_fields() {
		
		// USERS
		$table_users 	= 'users';
		$resource		= db_query("PRAGMA table_info({$table_users})");
		while($row = $resource->fetchAssoc()) $result[] = $row;
		
		foreach($result as $key => $value) {
				
				$params = array(
								'apikey' 		=>	$this->CONFIG['api_key'],
								'plugin_key'	=> $this->CONFIG['plugin_key'],
								'name' 			=>	$value['name'],
								'listID'		=>	$this->CONFIG['listid'],
								'type'			=>	$this->CONFIG['fields_type']);
				
				$result = $this->SOAP->addExtraField($params);
			
				$extra_fields[$value['name']] = $result['NEW_ID'];
			
		}
		
		// ROLES
		$roles[] = 'rid';
		$roles[] = 'weight';
		
		foreach($roles as $key => $value) {
			
			$params = array(
								'apikey' 		=>	$this->CONFIG['api_key'],
								'plugin_key'	=> $this->CONFIG['plugin_key'],
								'name' 			=>	$value,
								'listID'		=>	$this->CONFIG['listid'],
								'type'			=>	$this->CONFIG['fields_type']);
				
			$result = $this->SOAP->addExtraField($params);
			
			$extra_fields[$value] = $result['NEW_ID'];
		}
		
		#$truncate = db_query('TRUNCATE TABLE egoi_extra_field');
		
		db_query('DELETE FROM egoi_extra_field');
		
		foreach($extra_fields as $key => $value) {
		
			$insert = db_query('INSERT INTO egoi_extra_field (extra_field,coluna) VALUES (\''.$key.'\',\''.$value.'\')');
			
		}
		
		return true;
		
	}
	/**
	 * End E-GOI create extra fields
	 **/
	 
	 
	 
	/**
	* E-GOI puts users in the list
	**/ 
	
	/* ATENCAO
	 * 
	 * NOME,EMAIL,IDIOMA
	 * 
	 * */
	
	function egoi_addSubscriber() {
	
		
		$resource = db_query("	SELECT * 
								FROM `users`
								INNER JOIN `users_roles` 
								ON `users`.`uid` = `users_roles`.`uid`
								INNER JOIN `role`
								ON `users_roles`.`rid` = `role`.`rid`");
		
		while($row = $resource->fetchAssoc()) $result[] = $row;
		
		if(!(isset($result)))
			return false;
		
		
		
		// EXTRA FIELDS
		$extra = db_query("SELECT * FROM egoi_extra_field");
		while($row2 = $extra->fetchAssoc()) $rextra[] = $row2;
		
		
		foreach($result as $value) {
			
			$params 	= array();
			$new_array 	= array();
			
			$params['apikey'] 		= $this->CONFIG['api_key'];
			$params['plugin_key'] 	= $this->CONFIG['plugin_key'];
			$params['listID']		= $this->CONFIG['listid'];
			$params['status'] 		= 1;
			$params['name']			= $value['name'];
			$params['email'] 		= $value['mail'];
			
			
			foreach($value as $key => $value2) {
				$new_array[] = array( 'value' => $value2 );
			}	
			
			
			$i = 0;	
			foreach($rextra as $key => $value) {
				
				$new_array[$i]['extra'] = $value['coluna'];
				$i++;
			}
			
			
			foreach($new_array as $key => $value)
				$params['extra_'.$value['extra']] = $value['value'];
			
			$result = $this->SOAP->addSubscriber($params);
		}
		
		
	}	 
	/**
	* End E-GOI puts users in the list
	**/ 
	
	
	
	/**
	* E-GOI user cron
	**/ 
	function egoi_user_cron() {
	
		$conf = $this->egoi_get_configuration();
		
		$this->CONFIG['api_key'] 	= $conf['api_key'];
		$this->CONFIG['listid']		= $conf['list'];
		$this->egoi_addSubscriber();
	
	}
	/**
	* End E-GOI user cron
	**/ 



	/**
	* E-GOI get configuration
	**/ 
	function egoi_get_configuration() {
	
		$sql = db_query('SELECT * FROM egoi_configuration LIMIT 1');
		while($row = $sql->fetchAssoc()) $result = $row;
		
		return isset($result) ? $result:false;
	
	}
	/**
	* End E-GOI get configuration
	**/ 


	
	
}

?>
