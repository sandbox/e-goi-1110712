<?php

/**
 * @file
 * e-goi module admin settings.
 */


function egoi_admin_settings() {

	$EGOI = new EGOIAPI();
	
	
	// Verify if settings exists
	$sql 	= "SELECT * FROM {$EGOI->CONFIG['table_settings']} LIMIT 1";
	$result = db_query($sql);
	#$EGOI->CONFIG['settings_exists'] = $result->rowCount() ? true:false;

	/*if(!($EGOI->CONFIG['settings_exists'])) {
		$message = 'To use this module, you need to create the configuration.';
		drupal_set_message($message, 'warning');
	} else {
		$message = 'Your data are correct, please do not change them.';
		drupal_set_message($message);
	}*/



	// Fieldset
	$form['egoi_account_info'] = array(
		'#type'       	=> 'fieldset',
		'#collapsible' 	=> TRUE,
		'#collapsed' 	=> FALSE,
		'#title'      	=> 'e-goi settings',
	);
  
	// Field e-goi API Key
	$form['egoi_account_info']['egoi_api_key'] = array(  
		'#type' 			=> 'textfield',
		'#title' 			=> t('API Key'),
		'#required' 		=> TRUE,
		'#default_value' 	=> variable_get('egoi_api_key', ''),
		'#description' 	=> t('The API key for your E-GOI account. Get a valid API key at your !apilink.', array('!apilink' => l(t('E-GOI account'), $EGOI->CONFIG['bo_url'])))
	);
	
	// Field e-goi list
	$form['egoi_account_info']['egoi_list'] = array(  
		'#type' 			=> 'textfield',
		'#title' 			=> t('List name'),
		'#required' 		=> TRUE,
		'#default_value' 	=> variable_get('egoi_list', ''),
		'#description' 		=> t('Put a different name from their lists of your E-GOI account will be created as a new.')
	);
	
	// Field e-goi lang
	$langs = array( 
					"Deutsch",
					"English", 
					"Español",
					"Française",
					"Português (Brasil)",
					"Português (Portugal)");
	
	$form['egoi_account_info']['egoi_lang'] = array(  
		'#type' 			=> 'select',
		'#title' 			=> t('Language'),
		'#options'			=> $langs,
		'#description' 		=> t('Select your language to your list.'),
		'#required' => TRUE
	);
	
  
	// Validate form
	$form['#validate'][] = 'egoi_admin_settings_validate';
	

	return system_settings_form($form);
	
}

function egoi_admin_settings_validate($form, &$form_state) {
	
	
	
	// Fields
	$fields['api_key'] 		= trim($form['egoi_account_info']['egoi_api_key']['#value']);
	$fields['list_name']	= trim($form['egoi_account_info']['egoi_list']['#value']);
	$fields['lang']			= trim($form['egoi_account_info']['egoi_lang']['#value']);
	
	
	// New EGOI CLASS
	$EGOI = new EGOIAPI();
	$EGOI->egoi_new_soap();
	$EGOI->CONFIG['api_key_valid'] = $EGOI->egoi_verify_api_key($fields['api_key']);
	$EGOI->egoi_user_cron();
					$message = 'Error creating list.';
					form_set_error('egoi_list', t($message));
					return;
	// Verify API Key
	if(!(isset($form['egoi_account_info']['egoi_api_key']['#value']))) {
		
		$message = 'Please put your API Key.';
		form_set_error('egoi_api_key', t($message));
		return;
	} else {
		
		if(!($EGOI->CONFIG['api_key_valid'])) {
			
			$message = 'Invalid API Key.';
			form_set_error('egoi_api_key', t('Invalid API Key.'));
			return;
		} else {
			
			// ADD API Key to class EGOI
			$EGOI->CONFIG['api_key'] = $fields['api_key'];
		} 
	}
	
	
	// Verify List name
	if(!(isset($form['egoi_account_info']['egoi_list']['#value']))) {
		
		$message = 'Please put the name of your list.';
		form_set_error('egoi_list', t($message));
		return;
	} else {
		
		$match = '[A-Za-z0-9]';
		if((preg_match($match,$fields['list_name']))) {
			
			$message = 'The list name must be only letters and numbers.';
			form_set_error('egoi_list', t($message));
			return;
		} elseif(($EGOI->egoi_list_exists($fields['api_key'], $fields['list_name']))) {
			
			$message = 'The list name already exists.';
			form_set_error('egoi_list', t($message));
			return;
		} elseif(!(strlen($fields['list_name']))) {
			
			return;
		} else {
			
			// Lang
			if(!(isset($form['egoi_account_info']['egoi_api_key']['#value']))) {
				
				$message = 'Please put your API Key.';
				form_set_error('egoi_api_key', t($message));
				return;
			} elseif(!(strlen($fields['lang']))) {
				
				return;
			} else {
			
				// ADD LANG to EGOI CLASS
				$EGOI->CONFIG['lang'] = $fields['lang'];
				
			
				// Create list	
				$egoi_list = $EGOI->egoi_create_list($fields['list_name']);
				if($egoi_list) {
					
					// LIST ID
					$EGOI->CONFIG['listid'] = $egoi_list['LIST_ID'];
					$message = 'Your list was created successfully.';
					drupal_set_message($message);
						
						
					// Create extra fields
					if(!($EGOI->egoi_create_extra_fields())) {

						$message = 'Error creating extra fields.';
						form_set_error('egoi_list', t($message));
						return;
					}
					
					
					$EGOI->egoi_addSubscriber();
					
					db_query('DELETE FROM egoi_configuration');
					$ins = db_query('INSERT INTO egoi_configuration (api_key,list) VALUES (\''.$EGOI->CONFIG['api_key'].'\',\''.$EGOI->		CONFIG['listid'].'\')');
					
					
				} else {
				
					$message = 'Error creating list.';
					form_set_error('egoi_list', t($message));
					return;
				}
			
			}
			
		}
	}
	
	
	
}
